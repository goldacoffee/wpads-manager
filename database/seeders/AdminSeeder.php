<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Str;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // check if not exist 
        $x =  User::where('email', 'admin@ads.com')->first();
        if (!$x) {

            $x = new User();
            $x->id = Str::uuid();
            $x->name = 'admin';
            $x->email = 'admin@ads.com';
            $x->password = Hash::make('123');
            $x->save();
        }
    }
}
