<?php

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {

//     return $request->user();
// });


Route::post('save-banner-position', [ApiController::class, 'saveBannerPosition']);
Route::post('save-link-position', [ApiController::class, 'saveLinkPosition']);
Route::get('getAllBanner', [ApiController::class, 'getAllBanner']);
Route::post('deleteBannerConfirmation', [ApiController::class, 'deleteBannerConfirmation']);
Route::post('deleteJudiRekomendasi', [ApiController::class, 'deleteJudiRekomendasi']);
