<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashController;
use App\Http\Controllers\JudiRekomendasiController;
use App\Http\Middleware\DashboardMiddleware;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('auth')->group(function () {
    Route::get('login', [AuthController::class, 'login'])->name('show.login');
    Route::post('login', [AuthController::class, 'doLogin'])->name('do.login');
    Route::any('post', [AuthController::class, 'logout'])->name('do.logout');
});


Route::middleware([DashboardMiddleware::class])->group(function () {
    Route::prefix('dash')->group(function () {
        Route::get('', [DashController::class, 'index'])->name('dash.index');
        Route::get('banner/{bannertype}', [DashController::class, 'banner'])->name('dash.banner');
        Route::get('banner/{bannertype}/{bannerid}/edit', [DashController::class, 'bannerEdit'])->name('dash.banner.edit');
        Route::post('banner/{bannertype}/{bannerid}/edit', [DashController::class, 'bannerEdit'])->name('dash.banner.edit');
        Route::get('banner/{type}/add', [DashController::class, 'bannerAdd'])->name('dash.banner.add');
        Route::post('banner/{type}/add', [DashController::class, 'bannerAddAction'])->name('dash.banner.add.action');

        Route::prefix('judi-rekomendasi')->group(function () {
            Route::get('', [JudiRekomendasiController::class, 'index'])->name('judi_rekomendasi.index');
            Route::get('/add', [JudiRekomendasiController::class, 'add'])->name('judi_rekomendasi.add');
            Route::post('/add', [JudiRekomendasiController::class, 'doAdd'])->name('judi_rekomendasi.doAdd');
            Route::get('/{id}/edit', [JudiRekomendasiController::class, 'edit'])->name('judi_rekomendasi.edit');
        });
    });
});
