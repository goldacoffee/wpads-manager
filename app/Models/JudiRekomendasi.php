<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JudiRekomendasi extends Model
{
    use HasFactory;
    protected $table = 'judi_online_rekomendasi';
    protected $fillable = [
        "id",
        "name",
        "img",
        "url",
        'position',

    ];
    protected $primaryKey = 'id'; // or null

    public $incrementing = false;
}
