<?php

use App\Models\Banner;
use App\Models\JudiRekomendasi;

if (!function_exists('getBannerByType')) {
    function getBannerByType($type)
    {
        return Banner::where('type', $type)->orderBy('position', 'asc')->get();
    }
}
if (!function_exists('sidebarBannerMenu')) {
    function sidebarBannerMenu()
    {
        return [
            "majalahpro_core_top_banner",
            "majalahpro_core_top_banner_after_menu",
            "majalahpro_core_add_banner_inside_content",
            "majalahpro_core_add_banner_inside_content_other",
            "majalahpro_core_banner_before_content",
            "majalahpro_core_banner_after_content",
            "majalahpro_core_banner_after_sidebar",
            "majalahpro_core_floating_banner_footer",
            "majalahpro_core_floating_banner_left",
            "majalahpro_core_floating_banner_right",
            "majalahpro_core_floating_banner_popup_1",
            "majalahpro_core_floating_banner_popup_2",
            "majalahpro_core_floating_banner_popup_3",
            "majalahpro_core_floating_banner_popup_4",
        ];
    }
}

if (!function_exists('saveLinkRekomendasi')) {
    function saveLinkRekomendasi()
    {
        $data = JudiRekomendasi::orderBy('position', 'asc')->get();
     

        file_put_contents(public_path() . '/feed/judirekomendasi.json', json_encode($data));
    }
}
