@extends('dash.layout')
@section('content')
  <div class="judionline-rekomendasi__tambah_link">
    <h3>{{ $action === 'edit' ? 'Edit' : 'Tambah' }} Link Judi Online Rekomendasi List</h3>


    <form action="{{ route('judi_rekomendasi.doAdd') }}" class="row" method="POST">
      @csrf

      @if ($action === 'edit')
        <input type="hidden" name="id" value="{{ $data->id }}">
      @endif
      <input type="hidden" name="action" value="{{ $action }}">

      <div class="mb-1">
        <label for="img" class="form-label">Nama Provider </label>
        <input type="text" class="form-control" placeholder="" name="name" id="name" required
          value="{{ isset($data) ? $data->name : '' }}" autocomplete="off">
      </div>
      <div class="mb-1">
        <label for="img" class="form-label">Image Provider </label>
        <input type="text" class="form-control" placeholder="" name="img" id="img" required
          value="{{ isset($data) ? $data->img : '' }}" autocomplete="off">
      </div>
      <div class="mb-1">
        <label for="img" class="form-label">Link Provider </label>
        <input type="text" class="form-control" placeholder="" name="url" id="url" required
          value="{{ isset($data) ? $data->url : '' }}" autocomplete="off">
      </div>
      <div class="d-grid gap-2 mt-3">
        <input type="submit" class="btn  btn-success" value="ADD LINK">

      </div>
    </form>
  </div>

@endsection
