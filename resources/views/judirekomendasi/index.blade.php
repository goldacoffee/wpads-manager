@extends('dash.layout')
@section('content')
  <div class="judionline-rekomendasi">
    <h3>Judi Online Rekomendasi List</h3>
    <a href="{{ route('judi_rekomendasi.add') }}" class="btn btn-danger btn-sm tambah-banner-button mb-3">Tambah
      Link</a>
  </div>

  <table class="table table-striped table-hover table-responsive">
    <thead>
      <tr>

        <th class="text-center">NAME</th>
        <th class="text-center">IMG</th>
        <th class="text-center">LINK</th>
        <th class="text-center">ACTION</th>
      </tr>
    </thead>
    <tbody id="sortable-link" data-type="">
      @foreach ($data as $key => $item)
        <tr id="{{ $item->id }}">
          <td>{{ $item->name }}</td>
          <td class="text-center">{{ $item->img }}</td>
          <td class="text-center"><a href="{{ $item->url }}" class="btn btn-sm btn-primary" target="_blank">Click</a></td>
          <td class="text-center">
            <a href="{{ route('judi_rekomendasi.edit', ['id' => $item->id]) }}"
              class="jur-edit btn btn-sm  btn-info">Edit</a>
            <button class="jur-delete btn btn-sm btn-danger" data-id="{{ $item->id }}">Delete</button>

          </td>
        </tr>


      @endforeach

    </tbody>
  </table>



@endsection


@push('css')

  <link rel="stylesheet" href="/assets/plugins/izitoast/css/iziToast.min.css">
@endpush
@push('js')

  <script src="/assets/plugins/izitoast/js/iziToast.min.js"></script>



@endpush
