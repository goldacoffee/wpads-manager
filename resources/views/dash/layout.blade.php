<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>@yield('title',env('APP_NAME')) - Dashboard</title>
  <!-- CSS only -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
    crossorigin="anonymous">
  @stack('css')
  <link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>

<body>
  <div class="container">
    <div class="content-part">
      <div class="sidebar">
        @includeIf('dash.sidebar')
      </div>
      <div class="main-content">
        @yield('content')
      </div>

    </div>
  </div>

  <!-- JavaScript Bundle with Popper -->

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous">
    < script src = "https://code.jquery.com/jquery-3.6.0.min.js"
    integrity = "sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
    crossorigin = "anonymous" >
  </script>
  </script>
  @stack('js')
  <script src="{{ mix('js/app.js') }}"></script>

</body>

</html>
